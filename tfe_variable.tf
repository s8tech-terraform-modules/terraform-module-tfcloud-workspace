resource "tfe_variable" "aws_role_arn" {
  key          = "switch_role_arn"
  value        = var.switch_role_arn
  category     = "terraform"
  workspace_id = tfe_workspace.workspace.id
  sensitive    = false
  description  = "Switch Role ARN to Workspace"
  hcl          = false
}

resource "tfe_variable" "aws_role_session_name" {
  key          = "AWS_ROLE_SESSION_NAME"
  value        = "terraform-s8tech"
  category     = "env"
  workspace_id = tfe_workspace.workspace.id
  sensitive    = false
  description  = "Session Name to Switch Role"
  hcl          = false
}

resource "tfe_variable" "region" {
  key          = "region"
  value        = var.region
  category     = "terraform"
  workspace_id = tfe_workspace.workspace.id
  sensitive    = false
  description  = "AWS Region to Workspace"
  hcl          = false
}

resource "tfe_variable" "customer" {
  key          = "customer"
  value        = var.customer
  category     = "terraform"
  workspace_id = tfe_workspace.workspace.id
  sensitive    = false
  description  = "Customer to Workspace"
  hcl          = false
}

resource "tfe_variable" "variables" {
  for_each = { for cj in var.workspace_variables : cj.name => cj }

  key          = each.key
  value        = jsonencode(each.value.value)
  category     = "terraform"
  workspace_id = tfe_workspace.workspace.id
  sensitive    = lookup(each.value, "sensitive", false)
  hcl          = lookup(each.value, "hcl", true)
}