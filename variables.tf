variable "costumer" {
  
}

variable "account_name" {
  
}

variable "region" {
  
}

variable "organization" {
  type        = string
  default     = "s8tech"
  description = "Name of the organization."
}

variable "terraform_version" {
  type        = string
  default     = "1.0.0"
  description = "description"
}

variable "working_directory" {
  type        = string
  default     = null
  description = "description"
}

variable "trigger_prefixes" {
  type        = list(any)
  default     = null
  description = "description"
}

variable "vcs_identifier" {
  type        = string
  description = "description"
}

variable "vcs_branch" {
  type        = string
  description = "description"
}

variable "oauth_token_id" {
  type        = string
  description = "An OAuth Client represents the connection between an organization and a VCS provider"
}

variable "switch_role_arn" {
  
}

variable "customer" {
  default = ""
}


variable "project_id" {
  type        = string
  default     = null
}

variable "workspace_function" {
  type        = string
}

variable "workspace_variables" {
  default = []
}
