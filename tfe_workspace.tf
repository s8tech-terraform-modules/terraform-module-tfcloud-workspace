resource "tfe_workspace" "workspace" {
  name                  = format("%s_%s_%s_%s", var.costumer, var.account_name, var.workspace_function,var.region)
  organization          = var.organization
  file_triggers_enabled = false
  terraform_version     = var.terraform_version
  working_directory     = var.working_directory
  trigger_prefixes      = var.trigger_prefixes
  project_id            = var.project_id

  vcs_repo {
    identifier         = var.vcs_identifier
    branch             = var.vcs_branch
    ingress_submodules = true
    oauth_token_id     = var.oauth_token_id
  }
}
